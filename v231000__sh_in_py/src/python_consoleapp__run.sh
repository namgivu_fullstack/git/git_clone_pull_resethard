SH_=`cd $(dirname ${BASH_SOURCE:-$0}) && pwd`
cd $SH_

PYTHONPATH=$SH_ \
python -m pipenv run \
  python -u $SH_/'gitclone.py' 2>&1 | tee $SH_/'gitclone.py.md'
  #      -u to force nocache/flush=True when print()  ref https://stackoverflow.com/a/107720/248616

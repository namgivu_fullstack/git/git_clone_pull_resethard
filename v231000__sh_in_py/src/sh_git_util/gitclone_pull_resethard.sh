docstring=cat<<EOT
gitcloned_to_d=/path/to/gitcloned_to_d \
          repo=git@gitlab.com':owner_user_or_group'/path/to/reoname.git  \
        branch=main \
bash gitclone_pull_resethard.sh
EOT

[ -z $repo           ] && (echo 'Envvar repo is required'; kill $$)
[ -z $branch         ] && (echo 'Envvar branch is required'; kill $$)
[ -z $gitcloned_to_d ] && (echo 'Envvar gitcloned_to_d is required'; kill $$)

mkdir -p $gitcloned_to_d

cd $gitcloned_to_d
  if [[ ! -z $skipacceptblueprint ]]; then
    #         add gitlab.com to ~/.ssh/known-hosts sothat ssh command won't ask for 1sttimeconnect ip  ref https://serverfault.com/a/1090385/41015
    ssh-keygen -F gitlab.com || ssh-keyscan gitlab.com >> ~/.ssh/known_hosts  ;echo
    ssh-keygen -F github.com || ssh-keyscan github.com >> ~/.ssh/known_hosts  ;echo
    #         add github.com to ~/.ssh/known-hosts sothat ssh command won't ask for 1sttimeconnect ip  ref https://serverfault.com/a/1090385/41015
  fi

    gitclone_pull_resethard() {
        local v=$1 ; echo $v; repo=$v
        local v=$2 ; echo $v; branch=$v

                                                              reponame=`echo $repo | rev | cut -d'/' -f1 | rev`
                                                              reponame=${reponame%.*}
                                                                          branchname=`python3 -c "s='$branch'.replace('/', ''); print(s)" `  # replace / in $branch
                                               repo_n_branch="$reponame--$branchname"
        gitcloned_d="$gitcloned_to_d/$prefix--$repo_n_branch"

        echo -e "\n--- $branch @ $repo into $gitcloned_d"

        # git clone if not already so
        git clone -b $branch  $repo         $gitcloned_d  ;echo
        #         -b branch   repo_address  location_dir

        set -e  # halt if error ON
            cd $gitcloned_d

            # fetch latest remote if any
            git remote  update origin --prune ;echo

            # reset to latest branch           ; need to re-checkout branch to firm the branch name, justincase multi-branches at same gitref
            git reset --hard  "origin/$branch" ; git checkout $branch; echo

            # gitref                                             ; oneline log w/ timestamp  ref https://stackoverflow.com/questions/3631005/git-log-tabular-formatting https://g.co/bard/share/ad25e3db4efd
            echo ; git remote -v|head -n1 ; git branch | grep "*"; git log --format='%h %<(11)%s %<(22)%cr' -2
        set +e  # halt if error OFF
    }

    gitclone_pull_resethard $repo $branch

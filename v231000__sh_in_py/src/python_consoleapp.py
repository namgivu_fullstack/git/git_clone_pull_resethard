from pathlib import Path
import subprocess
import re


# input
SH=Path(__file__).parent

#        =       /your-ssh/key/here
ssh_key_f='~/.ssh/namgivu-github-ssh'

prefix         = 'anton-petrov'
gitcloned_to_d = str(SH/'zgitcloned')
repo           = 'https://github.com/pyenv/pyenv.git'
branch         = 'master'

### main

'''
run .sh in py ref https://replit.com/@NamG1/TOYA03-tel4vn#s230919_2040_week03_exercise_df.py
'''
print(f'{repo=:>}')
print(f'{branch=:>}')
print(f'{gitcloned_to_d=:>}')


##region run_gitclone()
def run_gitclone(repo):
  shcmd = f'''
              prefix={prefix}         \
      gitcloned_to_d={gitcloned_to_d} \
                repo={repo}           \
              branch={branch}         \
      GIT_TERMINAL_PROMPT=0  ssh-agent bash -c "ssh-add {ssh_key_f} ; {SH}/sh_git_util/gitclone_pull_resethard.sh" ; \
      echo; ls -l {gitcloned_to_d}
    '''.strip()
  # GIT_TERMINAL_PROMPT=0 for no prompt during the run  ref https://stackoverflow.com/a/64319771/248616

  o = subprocess.run(shcmd, capture_output=True, shell=True)
  # =           .run      ,                    , shell=True to use pipe | in command

  print(f'{o.returncode=}')
  print('\no.stderr.decode()\n', o.stderr.decode().strip())
  print('\no.stdout.decode()\n', o.stdout.decode().strip())

  return o

# proceed to git clone, implied that it is a public repo; if failed, we will try it as private repo afterward
o = run_gitclone(repo)

#region handle private repo if it is
# when failed maybe dueto private repo -> try @ git clone w/ ssh repo url git@github.com/xx/...
has_err = not o.returncode == 0
if has_err:
  print('== ERROR failed maybe dueto private repo -> try @ git clone w/ ssh repo url git@github.com/xx/...')

  m        = re.findall(r'https://github.com/(.+)/(.+)', repo)
  owner    = m[0][0]
  reponame = m[0][1]
  repo_ssh = f'git@github.com:{owner}/{reponame}'

  print(f'== retry @ git clone w/ ssh repo url {repo_ssh} ...')
  o2 = run_gitclone(repo_ssh)

  has_err2 = not o2.returncode == 0
  if has_err2:  # still has error record as failed
    raise Exception('ERROR cannot git your repo')
#endregion handle private repo if it is

##endregion run_gitclone()

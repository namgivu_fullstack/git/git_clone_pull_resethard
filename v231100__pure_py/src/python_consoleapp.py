from pathlib import Path
import git


def gitclonepull(repo, branch, gitcloned_path, prefix, sshkey=None):
  """
  ref https://stackoverflow.com/a/15388550/248616
  """
  reponame       = repo.split('/')[-1].split('.')[0]
  gitcloned_to_d = f'{gitcloned_path}/{prefix}--{reponame}-{branch}'
  env            ={'GIT_SSH_COMMAND': f'ssh -i {sshkey}'} if sshkey else {}

  print()
  print(f'{repo=:>}')
  print(f'{branch=:>}')
  print(f'{prefix=:>}')
  print(f'{gitcloned_to_d=:>}')
  print(f'{str(env)=:>}')

  ##region git helper

  #region progress stuff
  from git import RemoteProgress
  class CloneProgress(RemoteProgress):  # git clone w/ progress  ref https://stackoverflow.com/a/53999435/248616
    def update(self, op_code, cur_count, max_count=None, message=''):
      if message:
        print(message)
  #endregion progress stuff

  def gitclone():
    git.Repo.clone_from(url=repo, to_path=gitcloned_to_d, branch=branch, progress=CloneProgress(), env=env)

  def gitpull():
    '''
    # git.cmd.Git(gitcloned_to_d).pull()              # ref https://stackoverflow.com/a/15315667/248616
    # git.Repo(gitcloned_to_d).remotes.origin.pull()  # ref https://stackoverflow.com/a/53111508/248616
    '''
    git.Repo(gitcloned_to_d).remotes.origin.pull(progress=CloneProgress(), env=env)  # ref https://stackoverflow.com/a/53111508/248616

  ##endregion git helper


  try:
    print('--- proceed gitclone ...')
    gitclone()

  # if gitclone failed, try gitpull instead asif it already cloned
  except Exception as e:
    try:
      print('--- proceed gitpull ...')
      gitpull()

    except Exception as e:  # still fail, somthing wrong raised
      raise

  #TODO allow gitclonepull vs private repo ie w/ sshkey


if __name__=='__main__':
  SH=Path(__file__).parent

  #region sample repo @ pyenv
  repo   = 'https://github.com/pyenv/pyenv.git' ; reponame=repo.split('/')[-1].split('.')[0]
  branch = 'master'
  prefix = 'anton-petrov'

  gitclonepull(
    repo           = repo,
    branch         = branch,
    prefix         = prefix,
    gitcloned_path = str(SH/'zgitcloned'),
  )
  #endregion sample repo @ pyenv

  #region sample private repo @ namgivu_private
  repo   = 'git@gitlab.com:namgivu_fullstack/namgivu_private.git' ; reponame=repo.split('/')[-1].split('.')[0]
  branch = 'main'
  prefix = 'namgivu'
  # sshkey = '~/.ssh/namgivu-github-ssh'  # implied sshkey configured in ~/ssh/config, so we dontneed this param in this sample

  gitclonepull(
    repo           = repo,
    branch         = branch,
    prefix         = prefix,
    gitcloned_path = str(SH/'zgitcloned'),
  )
  #endregion sample private repo @ namgivu_private

  #region sample private repo at 2ndgitaccount onthemachine @ namgivu_private
  repo   = 'git@github.com:aibtxhsu/sampleprivaterepo.git' ; reponame=repo.split('/')[-1].split('.')[0]
  branch = 'main'
  prefix = 'aibtxhsu'

  sshkey = '~/.ssh/aibtxhsu-github'

  gitclonepull(
    repo           = repo,
    branch         = branch,
    prefix         = prefix,
    gitcloned_path = str(SH/'zgitcloned'),
    sshkey         = sshkey,
  )
  #endregion sample private repo at 2ndgitaccount onthemachine @ namgivu_private
